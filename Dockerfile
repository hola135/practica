FROM python
    RUN mkdir /practica
    
    WORKDIR /practica

    ADD . /practica

    RUN pip install -r requerimientos.txt