from django.db import models

class Persona(models.Model):
    numero=models.BigAutoField(primary_key=True) #clave primaria
    nombreyapellido=models.CharField(max_length=30)
    def __str__(self):
        texto='{}'.format(
            self.nombreyapellido)
        return texto


# Create your models here.
